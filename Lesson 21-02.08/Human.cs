﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_21_02._08
{
    abstract class Human
    {
        protected int age;
        public abstract string GetName();

        protected abstract void SetName(string name);
        

        public int GetAge()
        {
            return age;
        }

        public Human( int age)
        {
            this.age = age;
        }
    }
}
