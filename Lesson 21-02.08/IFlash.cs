﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_21_02._08
{
    interface IFlash:ISuperHero
    {
        void FireLightnings();
    }
}
