﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_21_02._08
{
    class SuperMan:Human, IFly
    {
        private int webLeft;

    public void SetWebLeft(int webLeft)
        {
            this.webLeft = webLeft;
        }

        public int GetWebLeft()
        {
            return webLeft;
        }

        private string name;

        public override string GetName()
        {
            return name;
        }

        protected override void SetName(string name)
        {
            this.name = name;
        }

        public void Fly()
        {
            Console.WriteLine("I can fly!");
        }

        public void ActivateSuperPowers()
        {
            Fly();
        }

        public SuperMan(string _name, int _age, int _webleft ):base( _age)
        {
            name = _name;
            webLeft = _webleft;
        }

        public override string ToString()
        {
            return $"[Name: ]:{name}, [Age: ]:{age}, [Webs left: ]:{webLeft} ";
        }
    }
}
