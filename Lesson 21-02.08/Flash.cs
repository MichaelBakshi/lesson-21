﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_21_02._08
{
    class Flash:Human, IFlash
    {
        private int voltage;

        public void SetSpeed(int _voltage)
        {
            this.voltage = _voltage;
        }

        public int GetVoltage()
        {
            return voltage;
        }

        private string name;

        public override string GetName()
        {
            return name;
        }

        protected override void SetName(string name)
        {
            this.name = name;
        }

        public void FireLightnings()
        {
            Console.WriteLine("I can fire lightnings!");
        }

        public void ActivateSuperPowers()
        {
            FireLightnings();
        }
        public Flash(string _name, int _age, int _voltage) : base(_age)
        {
            name = _name;
            voltage = _voltage;
        }
        public override string ToString()
        {
            return $"[Name: ]:{name}, [Age: ]:{age}, [Voltage: ]:{voltage} ";
        }
    }
}
