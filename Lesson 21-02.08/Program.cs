﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_21_02._08
{
    class Program
    {

        static void Main(string[] args)
        {
            SuperMan superman = new SuperMan("Clark", 32, 100);
            Spiderman spiderman = new Spiderman("Parker", 24, 500);
            Flash flash = new Flash("Jonh", 20, 4000);

            /*ISuperHero[] superHeroes = new ISuperHero[]            
            {
               superman,
               spiderman,
               flash
            };*/

            ISuperHero[] superHeroes = new ISuperHero[3];
            superHeroes[0] = superman;
            superHeroes[1] = spiderman;
            superHeroes[2] = flash;

            foreach (ISuperHero i in superHeroes)
            {
                ActivateHero(i);
                GetMoreHeroData(i);
            }
            Console.WriteLine(CreateHero("spiDErmaN"));
            Console.WriteLine(CreateHero("fLasH"));
            Console.WriteLine(CreateHero("sUPerMan"));
        }

        internal static void ActivateHero(ISuperHero superHero)
        {
            Console.WriteLine(superHero.ToString());
            superHero.ActivateSuperPowers();
        }

        internal static void IdentifyHero(ISuperHero superHero)
        {
            if (superHero is SuperMan superMan)
            {
                Console.WriteLine("Superman is detected");
            }
            else if (superHero is Spiderman spiderman)
            {
                Console.WriteLine("Spiderman is detected");
            }
            else if (superHero is Flash flash)
            {
                Console.WriteLine("Flash is detected");
            }
        }

        internal static void GetMoreHeroData(ISuperHero superHero)
        {
            if (superHero is Flash)
            {
                // way 1
                Flash flash = superHero as Flash;
                Console.WriteLine(flash.GetVoltage());

                //return;
                // way 2
                //Console.WriteLine(((Flash)superHero).GetVoltage());
            }

            if (superHero is SuperMan)
            {
                SuperMan superMan = superHero as SuperMan;
                Console.WriteLine(superMan.GetWebLeft());
            }

            if (superHero is Spiderman)
            {
                Spiderman spiderman = superHero as Spiderman;
                Console.WriteLine(spiderman.GetSpeed());
            }
        }
            static ISuperHero CreateHero(string superhero)
        {
            //ToUpper() or ToLower()
            if ("Superman".ToLower().Equals(superhero.ToLower()))
            {
                SuperMan superMan = new SuperMan("Your personal Superman", 25, 50);
                return superMan;
            }
            else if ("Spiderman".ToLower().Equals(superhero.ToLower()))
            {
                Spiderman spiderman = new Spiderman("Your personal Spiderman", 30, 100);
                return spiderman;
            }
            else if ("Flash".ToLower().Equals(superhero.ToLower()))
            {
                Flash flash = new Flash("Your personal Flash", 35, 200);
                return flash;
            }
            else
            {
                return null;
            }
        }
    }
}
