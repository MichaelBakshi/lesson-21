﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_21_02._08
{
    class Spiderman:Human, IClimb
    {
        private int speed;

        public void SetSpeed(int _speed)
        {
            this.speed = _speed;
        }

        public int GetSpeed()
        {
            return speed;
        }

        private string name;

        public override string GetName()
        {
            return name;
        }

        protected override void SetName(string name)
        {
            this.name = name;
        }

        public void Climb()
        {
            Console.WriteLine("I can climb!");
        }

        public void ActivateSuperPowers()
        {
            Climb();
        }
        public Spiderman(string _name, int _age, int _speed) : base(_age)
        {
            name = _name;
            speed = _speed;
        }
        public override string ToString()
        {
            return $"[Name: ]:{name}, [Age: ]:{age}, [Speed: ]:{speed} ";
        }
    }
}
